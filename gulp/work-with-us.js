'use strict';

const gulp = require('gulp');
const browserSync = require('browser-sync');
const babelify = require('babelify');
const browserify = require('browserify');
const vueify = require('vueify');
const source = require('vinyl-source-stream');
const $ = require('gulp-load-plugins')();

const injectFiles = gulp.src([
    'stylesheets/**/*.scss'
], { read: false });

const injectOptions = {
  transform: filePath => {
    return '@import \'' + filePath + '\';';
  },
  starttag: '// injector',
  endtag: '// endinjector',
  addRootSlash: false
};

gulp.task("workWithUsBundle", () => {
  return browserify({entries: ['js/work-with-us/main.js']})
    .transform(vueify)
    .transform(babelify, {"presets": ["es2015"]})
    .external('vue')
    .bundle()
    .pipe(source('bundle.js'))
    .pipe(gulp.dest('js/work-with-us'));
});

gulp.task("workWithUsConcat", ["workWithUsBundle"], () => {
   return gulp.src([
                'js/vue.min.js',
                'js/work-with-us/bundle.js'
                ])
            .pipe($.inject(injectFiles, injectOptions))
            .pipe($.sourcemaps.init())
            .pipe($.concat('ocm-work-with-us.js'))
            .pipe($.sourcemaps.write('./'))
            .pipe(gulp.dest('js'))
            .pipe(browserSync.reload({ stream: true }));
});
