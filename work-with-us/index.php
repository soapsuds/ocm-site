<!DOCTYPE html>
<html>
    <head>
        <?php include '../includes/ttu-head.html'; ?>
        <title>Work With Us | Office of Communications &amp; Marketing</title>
        <meta name="Description" content="">
    </head>
    <body>
        <?php include '../includes/ttu-body-top.php'; ?>
        <!-- CONTENT START -->
        <section class="ocm__wwu">
            <section class="ocm-wwu__notice">
                <div class="ocm-wwu-notice__title">
                    <h1>Project Request Notice</h1>
                    <img src="/images/work-with-us/calendar-icon.png" />
                </div>
                <div class="ocm-wwu-notice__text">
                    <h2>
                        Texas Tech University will be closed for winter break from Monday,
                        December 23, 2019 through Thursday, January 2, 2020. Please submit
                        all project requests prior to November 22nd for your best chance
                        to receive completed design projects before the break.
                    </h2>
                    <p>
                        <strong>Please note:</strong>
                        <br/>
                        <ul>
                            <li>Project completion is subject to receipt of content, revisions, and approvals in a timely manner.</li>
                            <li>These dates do not include process/set up time for external vendors.</li>
                            <li>Turn times for extensive design projects such as campaigns, booklets, photoshoots, and web development will be considered on a case by case basis.</li>
                        </ul>
                    </p>
                </div>
            </section>
            <section class="ocm-wwu__app"></section>
        </section>


<!--==================================
=            Main Content            =
===================================-->


   <!-- main questions -->

    <div class="project__text">
        <p class="ocm-wwu-project__question">Hi, Jonathan. How can we get in touch with you?</p>
        <p class="ocm-wwu-project__placeholder">youremail@ttu.edu</p>
    </div>


   <!-- next / continue -->

<div class="project__next--container">
    <a href="#" class="project__next">Continue</a>
</div>


     <!-- end  -->




  <!--  overall container end  -->




        <?php include '../includes/ttu-body-bottom.php'; ?>
    </body>
</html>
