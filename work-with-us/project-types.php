<!DOCTYPE html>
<html>
    <head>
        <?php include '../includes/ttu-head.html'; ?>
        <title>Work With Us | Office of Communications &amp; Marketing</title>
        <meta name="Description" content="">
    </head>
    <body>
        <?php include '../includes/ttu-body-top.php'; ?>




        <!-- CONTENT START -->
        <section class="ocm__wwu">
<div class="ocm-wwu-project__container">

<!--==================================
=           next / continue            =
===================================-->


<div class="project__next--container">
    <a href="#" class="project__next">Continue</a>
    <span class="next-section">Add Services</span>
</div>


     <!-- end  -->







<!--==================================
=          Project title / name           =
===================================-->


<div class="project__title--container">
    <span class="project-label">Project</span>
    <h3 class="project__title">Red Raider Meats</h3>
    
</div>


     <!-- end  -->


<!--==================================
=         Project Types         =
===================================-->

<div class="project__title--container">
 <h3 class="project__type">New Project</h3>
    <h3 class="project__type">Full Redesign</h3>
     <h3 class="project__type">Edit Existing</h3>


       <h3 class="project__type">Print</h3>
       <p class="project__type">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis condimentum pulvinar felis sit amet vulputate. Nam finibus quis arcu at tincidunt. </p>
            <h3 class="project__type">Digital</h3>
            <p class="project__type">Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis condimentum pulvinar felis sit amet vulputate. Nam finibus quis arcu at tincidunt. </p>
       <p>
</div>



  <!-- end  -->

</div>



  <!--  overall container end  -->

        </section>
      

        <?php include '../includes/ttu-body-bottom.php'; ?>
    </body>
</html>
