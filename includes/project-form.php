<?php
/*---Variables-------------------------------------*/
$office = 'Office of Communications & Marketing';
$subject = 'New Project Request';

// Project information
$name = strip_tags(trim($_POST['contactName']));
$email = filter_var($_POST['contactEmail'], FILTER_SANITIZE_EMAIL);
if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
  $response = array(
    'success' => false,
    'message' => $email . " is not a valid email address. Please go back and enter a valid email."
  );
  echo json_encode($response);
  die();
}
$department = strip_tags(trim($_POST['department']));
$projectName = strip_tags(trim($_POST['projectName']));
$projectType = strip_tags(trim($_POST['projectType']));
$projectCategory = $_POST['projectCategory'];
$projectServices = $_POST['projectServices'];
$additionalComments = strip_tags($_POST['comments']);

/*---Email to Office-------------------------------------*/
$headers = "From: " . $name . " <" . $email . ">\r\n";
$headers .= "Reply-To: " . $email . "\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

// Figuring out who to send the email to
$sendTo = 'michelle.hougland@ttu.edu, kuhrt.cowan@ttu.edu';
$designEmails = ', armando.godinez@ttu.edu, veronica.medina@ttu.edu, rony.dixon@ttu.edu, jake.a.young@ttu.edu, j.looney@ttu.edu';
$photoEmails = ', ashley.rodgers@ttu.edu, jeff.ramazani@ttu.edu, allison.hirth@ttu.edu';
$socialEmails = ', allison.matherly@ttu.edu';

// Looping through chosen categories and adding the appropriate emails
foreach ($projectCategory as $category) {
  if ($category === 'Design') {
    $sendTo .= $designEmails;
  } elseif ($category === 'Photography / Video') {
    $sendTo .= $photoEmails;
  } elseif ($category === 'Social Media') {
    $sendTo .= $socialEmails;
  }
}

// Setting the $to variable
$to = $sendTo;

// Message
$msg = '<html><body>';
$msg .= '<table width="100%" cellpadding="10">';
$msg .= "<tr style='background: #CC0000; color: #FFFFFF'><td colspan='2'><h1 style='color: #FFFFFF;'>Project Name: " . $projectName . "</h1></td></tr>";
$msg .= "<tr><td>Project Type:</td><td>" . $projectType . "</td></tr>";
$msg .= "<tr style='background: #EEEEEE;'><td>Project Categories:</td><td><ul>";
foreach ($projectCategory as $category) {
  $msg .= "<li>" . $category . "</li>";
}
$msg .= "</ul></td></tr>";
$msg .= "<tr><td>Project Services:</td><td><ul>";
foreach ($projectServices as $service) {
  $msg .= "<li>" . $service . "</li>";
}
$msg .= "</ul></td></tr>";
$msg .= "<tr style='background: #EEEEEE;'><td>Comments:</td><td>" . nl2br($additionalComments) . "</td></tr>";
$msg .= "<tr style='background: #CC0000; color: #FFFFFF'><td colspan='2'><h2 style='color: #FFFFFF;'>Contact Information</h2></td></tr>";
$msg .= "<tr><td>Name:</td><td>" . $name . "</td></tr>";
$msg .= "<tr style='background: #EEEEEE;'><td>Email:</td><td><a href=\"mailto:" . $email . "\">" . $email . "</a></td></tr>";
$msg .= "<tr><td>Department:</td><td>" . $department . "</td></tr>";
$msg .= "</table>";
$msg .= "</body></html>";

// Send Message
mail($to, $subject, $msg, $headers);
$response = array(
  'success' => true,
  'message' => 'Someone from our office will be contacting you shortly to begin the process.'
);
echo json_encode($response);
