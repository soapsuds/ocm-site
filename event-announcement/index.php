<!DOCTYPE html>
<html>
    <head>
        <?php include '../includes/ttu-head.html'; ?>
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>

        <title>Event Announcement | Office of Communications &amp; Marketing</title>
        <meta name="Description" content="">
    </head>
    <body>
        <?php include '../includes/ttu-body-top.php'; ?>




        <!-- CONTENT START -->
        <section class="ocm__event-announcement">
          <h1>Event Announcement</h1>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem accusamus magni hic nemo nobis sit ut. Suscipit, nihil atque officia repudiandae qui impedit, quo eius voluptatum, debitis iure molestiae delectus?</p>

          <?php
            if ($_SERVER["REQUEST_METHOD"] == "POST"):

            // Filtering out TTU security
            if (substr($email,0,25) === "no-reply-security@ttu.edu") {
            $headers = "From: Nope <" . $email . ">\r\n";
            $headers .= "Reply-To: " . $email . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

            $to = $email;

            // Message
            $msg = '<html><body>';
            $msg .= '<table width="100%" cellpadding="10">';
            $msg .= "<tr><td><img src=\"http://www.depts.ttu.edu/communications/logos/new/nope.gif\" alt=\"Ah ah ah\" /></td></tr>";
            $msg .= "</table>";
            $msg .= "</body></html>";

            // Send Message
            mail($to, 'Nope', $msg, $headers);

            die();
            }

            /*---Variables-------------------------------------*/
            $office = 'Office of Communications & Marketing';
            $subject = 'New Event Announcement';

            // Contact information
            $name = strip_tags(trim($_POST['contactName']));
            $title = strip_tags(trim($_POST['contactTitle']));
            $department = strip_tags(trim($_POST['contactDepartment']));
            $email = filter_var($_POST['contactEmail'], FILTER_SANITIZE_EMAIL);
            $email = trim($email);
            if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            echo("<p>" . $email . " is not a valid email address. Please go back and enter a valid email.</p>");
            die();
            }
            $phone = trim($_POST['contactPhone']);

            // Event information
            $eventWhat = strip_tags(trim($_POST['eventWhat']));
            $eventDate = $_POST['eventDate'];
            $eventTime = $_POST['eventTime'];
            $eventWhere = strip_tags(trim($_POST['eventWhere']));
            $eventDescription = strip_tags(trim($_POST['eventDescription']));

            /*---Email to the Writers-------------------------------------*/
            $headers = "From: " . $name . " <" . $email . ">\r\n";
            $headers .= "Reply-To: " . $email . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

            $to = "kuhrt.cowan@ttu.edu";

            // Message
            $msg = '<html><body>';
            $msg .= '<table width="100%" cellpadding="10">';
            $msg .= "<tr style='background: #CC0000; color: #FFFFFF'><td colspan='2'><h1 style='color: #FFFFFF;'>" . $title . "</h1></td></tr>";
            $msg .= "<tr style='background: #EEEEEE;'><td colspan='2'><h2>Contact info</h2></td></tr>";
            $msg .= "<tr><td>Name:</td><td>" . $name . "</td></tr>";
            if ($title !== '') {
                $msg .= "<tr><td>Title:</td><td>" . $title . "</td></tr>";
            }
            if ($department) {
                $msg .= "<tr><td>Department:</td><td>" . $department . "</td></tr>";
            }
            $msg .= "<tr><td>Email:</td><td><a href=\"mailto:" . $email . "\">" . $email . "</a></td></tr>";
            $msg .= "<tr><td>Phone Number:</td><td>" . $phone . "</td></tr>";
            $msg .= "<tr style='background: #EEEEEE;'><td colspan='2'><h2>Event Info</h2></td></tr>";
            $msg .= "<tr><td>What:</td><td>" . $eventWhat . "</td></tr>";
            $msg .= "<tr><td>Date:</td><td>" . $eventDate . "</td></tr>";
            $msg .= "<tr><td>Time:</td><td>" . $eventTime . "</td></tr>";
            $msg .= "<tr><td>Where:</td><td>" . $eventWhere . "</td></tr>";
            $msg .= "<tr><td>Description:</td><td>" . nl2br($eventDescription) . "</td></tr>";
            $msg .= "</table>";
            $msg .= "</body></html>";

            // Send Message
            mail($to, $subject, $msg, $headers);

          ?>

              <h3>Thank You</h3>
              <p>We have received your event announcement. We will contact you if we need anything else.</p>

          <?php else: ?>

              <form method="POST" class="ocm-event-announcement__form">
                  <fieldset>
                      <legend>Contact Information</legend>
                      <input type="text" name="contactName" placeholder="Contact Name" required>
                      <p class="ocm__error-tip">&nbsp;</p>
                      <input type="text" name="contactTitle" placeholder="Contact Title">
                      <p class="ocm__error-tip">&nbsp;</p>
                      <input type="text" name="contactDepartment" placeholder="Contact Department">
                      <p class="ocm__error-tip">&nbsp;</p>
                      <input type="email" name="contactEmail" placeholder="Contact Email" required>
                      <p class="ocm__error-tip">&nbsp;</p>
                      <input type="tel" name="contactPhone" placeholder="Contact Phone Number" required>
                      <p class="ocm__error-tip">&nbsp;</p>
                  </fieldset>
                  <fieldset>
                      <legend>Event Information</legend>
                      <input type="text" name="eventWhat" placeholder="What" required>
                      <p class="ocm__error-tip">&nbsp;</p>
                      <label for="eventDate">Date</label>
                      <input type="date" name="eventDate" required>
                      <p class="ocm__error-tip">&nbsp;</p>
                      <label for="eventTime">Time</label>
                      <input type="time" name="eventTime" required>
                      <p class="ocm__error-tip">&nbsp;</p>
                      <input type="text" name="eventWhere" placeholder="Where" required>
                      <p class="ocm__error-tip">&nbsp;</p>
                      <textarea name="eventDescription" placeholder="Description of the event." rows="8" cols="80" required></textarea>
                      <p class="ocm__error-tip">&nbsp;</p>
                  </fieldset>
                  <div id='recaptcha' class="g-recaptcha"
                      data-sitekey="6LeN6jQUAAAAAE1cuuOcAwzucqUiF5UiTHYmzE4L"
                      data-callback="submitNewEventAnnouncement"
                      data-size="invisible"></div>
                  <button type="submit" name="button">Send</button>
              </form>
          <?php endif; ?>
        </section>
        <!-- CONTENT END -->




        <?php include '../includes/ttu-body-bottom.php'; ?>
    </body>
</html>
