<!DOCTYPE html>
<html>
    <head>
        <?php include '../includes/ttu-head.html'; ?>
        <title>Logos | Office of Communications &amp; Marketing</title>
        <meta name="Description" content="">
    </head>
    <body>
        <?php include '../includes/ttu-body-top.php'; ?>




        <!-- CONTENT START -->
        <section class="ocm__logo ocm__logo--home">
          <h1>Lockup Request</h1>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem accusamus magni hic nemo nobis sit ut. Suscipit, nihil atque officia repudiandae qui impedit, quo eius voluptatum, debitis iure molestiae delectus?</p>
          <section class="ocm-logo-home__buttons">
              <a href="/logo/download/" class="ocm__more-link">Download Current Lockup</a>
              <a href="/logo/new/" class="ocm__more-link">New Organization Lockup</a>
          </section>
        </section>
        <!-- CONTENT END -->




        <?php include '../includes/ttu-body-bottom.php'; ?>
    </body>
</html>
