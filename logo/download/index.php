<!DOCTYPE html>
<html>
    <head>
        <?php include '../../includes/ttu-head.html'; ?>
        <title>Download | Logos | Office of Communications &amp; Marketing</title>
        <meta name="Description" content="">
    </head>
    <body>
        <?php include '../../includes/ttu-body-top.php'; ?>




        <!-- CONTENT START -->
        <section class="ocm__logo ocm__logo--download">
            <h1>Download Current Lockup</h1>
            <section class="ocm-logo-download__steps">
                <h2>Step 1:</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error culpa vel officia suscipit labore assumenda voluptatum magni inventore, sint totam eaque iure, accusamus. Error minima deserunt quia, tempora sequi quo.</p>

                <h2>Step 2:</h2>
                <p>Quis cursus tortor hendrerit. Sed ultrices nisl nibh, eu mattis augue rutrum nec.</p>

                <h2>Step 3:</h2>
                <a href="https://sharepoint13.itts.ttu.edu/sites/communications/logos/_layouts/15/start.aspx#/Shared%20Documents/Forms/AllItems.aspx?RootFolder=%2Fsites%2Fcommunications%2Flogos%2FShared%20Documents%2FDouble%20T&FolderCTID=0x01200054A003B172D16B43BDB3C8C675358185&View=%7BF85276E0%2DD36E%2D4102%2DA99B%2DE99937AF36BA%7D">Search Site</a>
            </section>
        </section>
        <!-- CONTENT END -->




        <?php include '../../includes/ttu-body-bottom.php'; ?>
    </body>
</html>
