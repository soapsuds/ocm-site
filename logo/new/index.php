<!DOCTYPE html>
<html>
    <head>
        <?php include '../../includes/ttu-head.html'; ?>
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>

        <title>New | Logos | Office of Communications &amp; Marketing</title>
        <meta name="Description" content="">
    </head>
    <body>
        <?php include '../../includes/ttu-body-top.php'; ?>




        <!-- CONTENT START -->
        <section class="ocm__logo ocm__logo--new">
            <h1>New Organization Lockup</h1>
            <div class="ocm-logo-form__container">
                <?php
                if ($_SERVER["REQUEST_METHOD"] == "POST"):

                    /*---Variables-------------------------------------*/
                    $office = 'Office of Communications & Marketing';
                    $subject = 'New Logo Request';

                    // Logo information
                    $department = strip_tags(trim($_POST['departmentName']));
                    $name = strip_tags(trim($_POST['contact']));
                    $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
                    $email = trim($email);
                    if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
                    echo("<p>" . $email . " is not a valid email address. Please go back and enter a valid email.</p>");
                    die();
                    }

                    // Filtering out TTU security
                    if (substr($email,0,25) === "no-reply-security@ttu.edu") {
                      $headers = "From: Nope <" . $email . ">\r\n";
                      $headers .= "Reply-To: " . $email . "\r\n";
                      $headers .= "MIME-Version: 1.0\r\n";
                      $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

                      $to = $email;

                      // Message
                      $msg = '<html><body>';
                      $msg .= '<table width="100%" cellpadding="10">';
                      $msg .= "<tr><td><img src=\"http://www.depts.ttu.edu/communications/logos/new/nope.gif\" alt=\"Ah ah ah\" /></td></tr>";
                      $msg .= "</table>";
                      $msg .= "</body></html>";

                      // Send Message
                      mail($to, 'Nope', $msg, $headers);

                      die();
                    }

                    /*---Email to Manager-------------------------------------*/
                    $headers = "From: " . $name . " <" . $email . ">\r\n";
                    $headers .= "Reply-To: " . $email . "\r\n";
                    $headers .= "MIME-Version: 1.0\r\n";
                    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

                    $to = "kuhrt.cowan@ttu.edu";

                    // Message
                    $msg = '<html><body>';
                    $msg .= '<table width="100%" cellpadding="10">';
                    $msg .= "<tr style='background: #CC0000; color: #FFFFFF'><td colspan='2'><h1 style='color: #FFFFFF;'>Logo For " . $department . "</h1></td></tr>";
                    $msg .= "<tr style='background: #EEEEEE;'><td>Contact info:</td><td>" . $name . " - <a href=\"mailto:" . $email . "\">" . $email . "</a></td></tr>";
                    $msg .= "</table>";
                    $msg .= "</body></html>";

                    // Send Message
                    mail($to, $subject, $msg, $headers);

                    ?>

                    <h2>Thank You</h2>
                    <p>
                    We have received your request for a new logo. If you have any questions or need anything else, please contact <a href="mailto:michelle.hougland@ttu.edu">Michelle Hougland</a>.
                    </p>

                <?php else: ?>
                    <form class="ocm-logo__form" method="POST">
                        <fieldset>
                            <input type="text" name="departmentName" placeholder="Name of Department" required>
                            <p class="ocm__error-tip">&nbsp;</p>
                            <input type="text" name="contact" placeholder="Point of Contact" required>
                            <p class="ocm__error-tip">&nbsp;</p>
                            <input type="email" name="email" placeholder="Email" required>
                            <p class="ocm__error-tip">&nbsp;</p>
                        </fieldset>
                        <div id='recaptcha' class="g-recaptcha"
                          data-sitekey="6LeN6jQUAAAAAE1cuuOcAwzucqUiF5UiTHYmzE4L"
                          data-callback="submitNewLogoForm"
                          data-size="invisible"></div>
                        <button type="submit" name="button">Send</button>
                    </form>
                <?php endif; ?>
            </div>
        </section>
        <!-- CONTENT END -->




        <?php include '../../includes/ttu-body-bottom.php'; ?>
    </body>
</html>
