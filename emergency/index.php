<!DOCTYPE html>
<html>
    <head>
        <?php include '../includes/ttu-head.html'; ?>
        <title>Emergency | Office of Communications &amp; Marketing</title>
        <meta name="Description" content="">
    </head>
    <body>
        <?php include '../includes/ttu-body-top.php'; ?>

        <!-- CONTENT START -->
        <?php
        // ini_set('display_errors',1); error_reporting(E_ALL);
        // Getting the emergency feed and turning it into an array
        $emergencyXml = simplexml_load_string(file_get_contents('no-alert.xml'));
        $emergencyJson = json_encode($emergencyXml);
        $emergencyFeed = json_decode($emergencyJson, TRUE);

        // Setting a bool that says if there's an emergency message or not
        $emergencyExists = array_key_exists('item', $emergencyFeed['channel']);

        $emergencyItems = [];

        if (array_key_exists(0, $emergencyFeed['channel']['item'])) {
            foreach ($emergencyFeed['channel']['item'] as $item) {
                $emergencyItems[] = $item;
            }
        } elseif (array_key_exists('title', $emergencyFeed['channel']['item'])) {
            $emergencyItems[] = $emergencyFeed['channel']['item'];
        }

        function getRSS($url) {
            $ch = curl_init();
            curl_setopt ($ch, CURLOPT_URL, $url);
            curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
            $contents = curl_exec($ch);
            if (curl_errno($ch)) {
              // echo curl_error($ch);
              // echo "\n<br />";
              $contents = '';
            } else {
              curl_close($ch);
            }

            if (!is_string($contents) || !strlen($contents)) {
            // echo "Failed to get contents.";
            $contents = '';
            }

            return $contents;
        }
        ?>
        <section class="ttu-emergency ttu-emergency-home">
          <div class="ttu-emergency-home__header">
              <h1>Emergency</h1>
              <p class="ttu-emergency-home-header__subtitle">Communications <span>Center</span></p>
              <p class="ttu-emergency-home-header__text">This site is intended to inform the Texas Tech University community about what to do in advance of, during and after an emergency.</p>
          </div>
          <section class="ttu-emergency-home__alert">
              <div class="ttu-emergency-home-alert__text">
                  <?php if($emergencyExists): ?>
                      <?php foreach($emergencyItems as $item): ?>
                          <div class="ttu-emergency-home-alert-text__slide">
                              <p class="ttu-emergency-home-alert__date"><?php echo date("n/j/y g:ia", strtotime($item['pubDate'])); ?></p>
                              <h2><?php echo $item['title']; ?></h2>
                              <p>
                                  <?php echo $item['description']; ?>
                              </p>
                          </div>
                      <?php endforeach; ?>
                  <?php else: ?>
                      <div class="ttu-emergency-home-alert-text__slide">
                          <p class="ttu-emergency-home-alert__date"><?php echo date("n/j/y"); ?></p>
                          <h2>No Message</h2>
                          <p>There is currently no message. If there was an emergency on campus, this website would hold pertinent information.</p>
                      </div>
                  <?php endif; ?>
              </div>
              <?php echo '<div class="ttu-emergency-home-alert__logo' . ($emergencyExists ? ' active' : ' ') . '">'; ?>
                  <img src="/images/emergency/tech-alert-logo.svg" alt="Tech Alert Logo" />
              <?php echo "</div>"; ?>
          </section>
          <section class="ttu-emergency-home__connect">
              <h2>Connect With Us</h2>
              <?php echo '<a href="https://www.facebook.com/TexasTechYou">facebook</a>'; ?>
              <?php echo '<a href="https://twitter.com/TexasTech">twitter</a>'; ?>
          </section>
          <section class="ttu-emergency-home__grid">
              <section class="ttu-emergency-home__body">
                  <h2>TechAlert Emergency Communications System</h2>
                  <p>Texas Tech uses TechAlert to communicate important alerts and emergency response information to students, faculty and staff. <a href="http://www.ttu.edu/emergencyalert">Update your contact information here.</a></p>
                  <img src="/images/emergency/snow-day-blue.jpg" alt="TTU Admin building with snow on it" />
                  <p>For outside groups who would like to receive emergency alerts, please email <a href="mailto:techalert@ttu.edu">techalert@ttu.edu</a>.</p>
                  <h2>Building Emergency Plans</h2>
                  <p>Emergency Action Plans (EAPs) are meant to inform occupants of buildings of what to do during an emergency. Download and read the EAPs for the buildings you occupy <a href="http://www.depts.ttu.edu/communications/emergency/emergencyplans/">here</a>.</p>
                  <h2>Active Shooter Training</h2>
                  <p>Lt. Eric Williams of the Texas Tech Police Department provides guidance for surviving an active shooter situation. It is available to all faculty, staff and students. Be prepared, <a href="http://www.depts.ttu.edu/ttpd/active_shooter.php">watch it today</a>.</p>
              </section>
              <section class="ttu-emergency-home__contact">
                  <h2>Important Contact Information</h2>
                  <p>In an emergency, call 911 for ambulance, fire or police. <a href="http://www.depts.ttu.edu/ttpd/">Texas Tech Police</a> non-emergency line <a href="tel:8067423931">806 742 3931</a>.</p>
                  <h4>Maintenance Emergencies</h4>
                  <?php echo '<a href="tel:8067424677" class="ttu-emergency-home-contact__number">806 742 4OPS</a>'; ?>
                  <h4>Information Technology</h4>
                  <?php echo '<a href="tel:8067424951" class="ttu-emergency-home-contact__number">806 742 HELP</a>'; ?>
                  <h4>Server-Related Issues</h4>
                  <?php echo '<a href="tel:8067423649" class="ttu-emergency-home-contact__number">806 742 3649</a>'; ?>
                  <h4>TTU Emergency Management Coordinator</h4>
                  <p class="ttu-emergency-home-contact__person">Ronald Phillips</p>
                  <?php echo '<a href="tel:8067422121" class="ttu-emergency-home-contact__number">806 742 2121</a>'; ?>
                  <h4>TTUS Emergency Management Coordinator</h4>
                  <p class="ttu-emergency-home-contact__person">Steve Bryant</p>
                  <?php echo '<a href="tel:8067420212" class="ttu-emergency-home-contact__number">806 742 0212</a>'; ?>
              </section>
          </section>
        </section>
        <!-- CONTENT END -->




        <?php include '../includes/ttu-body-bottom.php'; ?>
    </body>
</html>
