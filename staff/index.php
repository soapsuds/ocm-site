<!DOCTYPE html>
<html>
    <head>
        <?php include '../includes/ttu-head.html'; ?>
        <title>Staff | Office of Communications &amp; Marketing</title>
        <meta name="Description" content="">
    </head>
    <body>
        <?php include '../includes/ttu-body-top.php'; ?>




        <!-- CONTENT START -->
        <section class="ocm__staff">
            <section class="ocm-staff__header">
                <p class="ocm-staff-header__subtitle">Inspiring</p>
                <h1>Staff</h1>
                <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis condimentum pulvinar felis sit amet vulputate. Nam finibus quis arcu at tincidunt. </p>
            </section>

            <section class="ocm-staff__leadership">
                <h2 class="ocm-staff__h2">OC&amp;M Leadership</h2>
                <div class="ocm-staff__container">
                    <div class="ocm-staff-leadership__info">
                        <h4>Chris Cook</h4>
                        <p>Managing Director of the Office of Communications &amp; Marketing</p>
                    </div>
                    <div class="ocm-staff-leadership__photo" style="background-image: url('/communications/images/staff/chris-c.jpg');">
                        <a href="mailto:chris.cook@ttu.edu">Email</a>
                    </div>
                    <div class="ocm-staff-leadership__photo" style="background-image: url('/communications/images/staff/michelle.jpg');">
                        <a href="mailto:michelle.hougland@ttu.edu">Email</a>
                    </div>
                    <div class="ocm-staff-leadership__info">
                        <h4>Michelle Hougland</h4>
                        <p>Assistant Managing Director of the Office of Communications &amp; Marketing</p>
                    </div>
                </div>
            </section>

            <section class="ocm-staff__media">
                <h2 class="ocm-staff__h2">OC&amp;M<br />Media Relations</h2>
                <div class="ocm-staff__container">
                    <div class="ocm-staff__member">
                        <div class="ocm-staff-member__photo" style="background-image: url('/communications/images/staff/jeff.jpg');">
                            <a href="mailto:jeff.ramazani@ttu.edu">Email</a>
                        </div>
                        <div class="ocm-staff-member__info">
                            <h4>Jeff Ramazani</h4>
                            <p>Senior Producer</p>
                        </div>
                    </div>
                    <div class="ocm-staff__member">
                        <div class="ocm-staff-member__photo" style="background-image: url('/communications/images/staff/allison-h.jpg');">
                            <a href="mailto:allison.hirth@ttu.edu">Email</a>
                        </div>
                        <div class="ocm-staff-member__info">
                            <h4>Allison Hirth</h4>
                            <p>Senior Editor / Brodcast Writer</p>
                        </div>
                    </div>
                    <div class="ocm-staff__member">
                        <div class="ocm-staff-member__photo" style="background-image: url('/communications/images/staff/george.jpg');">
                            <a href="mailto:george.watson@ttu.edu">Email</a>
                        </div>
                        <div class="ocm-staff-member__info">
                            <h4>George Watson</h4>
                            <p>Senior Editor</p>
                        </div>
                    </div>
                    <div class="ocm-staff__member">
                        <div class="ocm-staff-member__photo" style="background-image: url('/communications/images/staff/karin.jpg');">
                            <a href="mailto:karin.slyker@ttu.edu">Email</a>
                        </div>
                        <div class="ocm-staff-member__info">
                            <h4>Karin Slyker</h4>
                            <p>Senior Editor</p>
                        </div>
                    </div>
                    <div class="ocm-staff__member">
                        <div class="ocm-staff-member__photo" style="background-image: url('/communications/images/staff/glenys.jpg');">
                            <a href="mailto:glenys.young@ttu.edu">Email</a>
                        </div>
                        <div class="ocm-staff-member__info">
                            <h4>Glenys Young</h4>
                            <p>Senior Editor</p>
                        </div>
                    </div>
                    <div class="ocm-staff__member">
                        <div class="ocm-staff-member__photo" style="background-image: url('/communications/images/staff/amanda.jpg');">
                            <a href="mailto:amanda.castro-crist@ttu.edu">Email</a>
                        </div>
                        <div class="ocm-staff-member__info">
                            <h4>Amanda Castro-Crist</h4>
                            <p>Senior Editor</p>
                        </div>
                    </div>
                    <div class="ocm-staff__member">
                        <div class="ocm-staff-member__photo">
                            <a href="mailto:amanda.bowman@ttu.edu">Email</a>
                        </div>
                        <div class="ocm-staff-member__info">
                            <h4>Amanda Bowman</h4>
                            <p>Senior Producer</p>
                        </div>
                    </div>
                </div>
            </section>

            <section class="ocm-staff__marketing">
                <h2 class="ocm-staff__h2">OC&amp;M Marketing</h2>
                <div class="ocm-staff__container">
                    <div class="ocm-staff__member">
                        <div class="ocm-staff-member__photo" style="background-image: url('/communications/images/staff/rony.jpg');">
                            <a href="mailto:rony.dixon@ttu.edu">Email</a>
                        </div>
                        <div class="ocm-staff-member__info">
                            <h4>Rony Dixon</h4>
                            <p>Graphic Designer</p>
                        </div>
                    </div>
                    <div class="ocm-staff__member">
                        <div class="ocm-staff-member__photo" style="background-image: url('/communications/images/staff/veronica.jpg');">
                            <a href="mailto:veronica.medina@ttu.edu">Email</a>
                        </div>
                        <div class="ocm-staff-member__info">
                            <h4>Veronica Medina</h4>
                            <p>Graphic Designer</p>
                        </div>
                    </div>
                    <div class="ocm-staff__member">
                        <div class="ocm-staff-member__photo" style="background-image: url('/communications/images/staff/armando.jpg');">
                            <a href="mailto:armando.godinez@ttu.edu">Email</a>
                        </div>
                        <div class="ocm-staff-member__info">
                            <h4>Armando Godinez</h4>
                            <p>Graphic Designer</p>
                        </div>
                    </div>
                    <div class="ocm-staff__member">
                        <div class="ocm-staff-member__photo" style="background-image: url('/communications/images/staff/jonathan.jpg');">
                            <a href="mailto:j.looney@ttu.edu">Email</a>
                        </div>
                        <div class="ocm-staff-member__info">
                            <h4>Jonathan Looney</h4>
                            <p>Web Designer</p>
                        </div>
                    </div>
                    <div class="ocm-staff__member">
                        <div class="ocm-staff-member__photo" style="background-image: url('/communications/images/staff/jake.jpg');">
                            <a href="mailto:jake.a.young@ttu.edu">Email</a>
                        </div>
                        <div class="ocm-staff-member__info">
                            <h4>Jake Young</h4>
                            <p>Web Designer</p>
                        </div>
                    </div>
                    <div class="ocm-staff__member">
                        <div class="ocm-staff-member__photo" style="background-image: url('/communications/images/staff/kuhrt.jpg');">
                            <a href="mailto:kuhrt.cowan@ttu.edu">Email</a>
                        </div>
                        <div class="ocm-staff-member__info">
                            <h4>Kuhrt Cowan</h4>
                            <p>Web Developer</p>
                        </div>
                    </div>
                    <div class="ocm-staff__member">
                        <div class="ocm-staff-member__photo" style="background-image: url('/communications/images/staff/allison-m.jpg');">
                            <a href="mailto:allison.matherly@ttu.edu">Email</a>
                        </div>
                        <div class="ocm-staff-member__info">
                            <h4>Allison Matherly</h4>
                            <p>Coordinator of Digital Engagement</p>
                        </div>
                    </div>
                    <div class="ocm-staff__member">
                        <div class="ocm-staff-member__photo" style="background-image: url('/communications/images/staff/ashley.jpg');">
                            <a href="mailto:ashley.rodgers@ttu.edu">Email</a>
                        </div>
                        <div class="ocm-staff-member__info">
                            <h4>Ashley Rodgers</h4>
                            <p>Photographer</p>
                        </div>
                    </div>
                </div>
            </section>

            <section class="ocm-staff__administration">
                <h2 class="ocm-staff__h2">OC&amp;M Administration</h2>
                <div class="ocm-staff__container">
                    <div class="ocm-staff__member">
                        <div class="ocm-staff-member__photo">
                            <a href="mailto:susie.land@ttu.edu">Email</a>
                        </div>
                        <div class="ocm-staff-member__info">
                            <h4>Susie Land</h4>
                            <p>Unit Coordinator</p>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        <!-- CONTENT END -->




        <?php include '../includes/ttu-body-bottom.php'; ?>
    </body>
</html>
