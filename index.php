<!DOCTYPE html>
<html>
    <head>
        <?php include 'includes/ttu-head.html'; ?>
        <title></title>
        <meta name="Description" content="">
    </head>
    <body>
        <?php include 'includes/ttu-body-top.php'; ?>
        <!-- CONTENT START -->
            <section id="ocm-home__header">
                <p class="ocm-home-header__subtitle">Inspiring</p>
                <h1>
                    <span>Stories</span>
                    <span>Ideas</span>
                    <span>Spirit</span>
                    <span>Futures</span>
                </h1>
                <p class="ocm-home-header__text">
                  We help Texas Tech tell its story to the rest of the world. Our team includes media relations, broadcast, graphic design, marketing, photography and Web development and publishing experts. How can we help you?
                </p>
            </section>

            <section id="ocm-home__ideas">
              <?php
              $ocmWork = [
                [
                  'title' => 'Lalo Alcaraz Evite',
                  'image' => 'Diversity-Alcaraz-Evite.png',
                  'categories' => 'Evite, Poster, Print'
                ],
                [
                  'title' => 'Civil Counterpoints',
                  'image' => 'civil-counterpoints.png',
                  'categories' => 'Poster, Website, Invite'
                ],
                [
                  'title' => 'Double-T College',
                  'image' => 'DoubleTCollege-Chess.png',
                  'categories' => 'Poster, Invite'
                ],
                [
                  'title' => 'Double-T College',
                  'image' => 'DoubleTCollege-FactOrFake.png',
                  'categories' => 'Poster, Invite'
                ],
                [
                  'title' => 'Enrollment',
                  'image' => 'EnrollmentInfographic.png',
                  'categories' => 'Infographic, Website'
                ]
              ];

              // Getting the first work
              $firstWork = $ocmWork[randomNumber($ocmWork)];
              // Getting the second work
              $secondWork = $ocmWork[randomNumber($ocmWork)];
              // Making sure the work is different
              while ($firstWork['title'] == $secondWork['title']) {
                  // Changing the work if it matches
                  $secondWork = $ocmWork[randomNumber($ocmWork)];
              }
              $thirdWork = $ocmWork[randomNumber($ocmWork)];
              // Making sure the work is different
              while ($firstWork['title'] == $thirdWork['title'] || $secondWork['title'] == $thirdWork['title']) {
                  // Changing the work if it matches
                  $thirdWork = $ocmWork[randomNumber($ocmWork)];
              }

              // Returns a random number between 0 and the length of the array
              function randomNumber($array) {
                  return rand(0, count($array) - 1);
              }
              ?>
                <h2 class="ocm-home__h2">Print &amp; Publications</h2>
                <div class="ocm-ideas__grid">
                    <div class="ideas-grid__idea">
                        <?php
                        echo "<div class=\"idea__image\" style=\"background-image: url('images/home/graphic-design/" . $firstWork['image'] . "');\"></div>";
                        ?>
                        <h4><?php echo $firstWork['title']; ?></h4>
                        <p><?php echo $firstWork['categories']; ?></p>
                    </div>
                    <div class="ideas-grid__idea">
                        <?php
                        echo "<div class=\"idea__image\" style=\"background-image: url('images/home/graphic-design/" . $secondWork['image'] . "');\"></div>";
                        ?>
                        <h4><?php echo $secondWork['title']; ?></h4>
                        <p><?php echo $secondWork['categories']; ?></p>
                    </div>
                    <div class="ideas-grid__idea">
                        <?php
                        echo "<div class=\"idea__image\" style=\"background-image: url('images/home/graphic-design/" . $thirdWork['image'] . "');\"></div>";
                        ?>
                        <h4><?php echo $thirdWork['title']; ?></h4>
                        <p><?php echo $thirdWork['categories']; ?></p>
                    </div>
                </div>
            </section>

            <section id="ocm-home__stories">
                <h2 class="ocm-home__h2">News &amp;<br />Stories</h2>
                <a href="http://today.ttu.edu/" target="_blank" class="ocm__more-link">Texas<br />Tech Today</a>

                <div class="home-stories__sliders">
                    <div class="home-stories__main-slider">
                        <div class="home-stories__main-slide">
                            <div class="home-stories-slide__image" style="background-image: url('http://today.ttu.edu/posts/2017/10/Images/ligo/LIGO-Corsi-2017-Conception-lg.jpg');"></div>
                        </div>
                        <div class="home-stories__main-slide">
                            <div class="home-stories-slide__image" style="background-image: url('http://today.ttu.edu/posts/2017/10/Images/101617-CASNR-FBRI-Micro-Gin-2-med.jpg');"></div>
                        </div>
                        <div class="home-stories__main-slide">
                            <div class="home-stories-slide__image" style="background-image: url('http://today.ttu.edu/posts/2017/09/Images/Changxue-Xu_lab_23a-med.jpg');"></div>
                        </div>
                    </div>

                    <div class="home-stories__second-slider">
                        <div class="home-stories__second-slide">
                            <div class="home-stories-slide__image" style="background-image: url('http://today.ttu.edu/posts/2017/10/Images/101617-CASNR-FBRI-Micro-Gin-2-med.jpg');"></div>
                        </div>
                        <div class="home-stories__second-slide">
                            <div class="home-stories-slide__image" style="background-image: url('http://today.ttu.edu/posts/2017/09/Images/Changxue-Xu_lab_23a-med.jpg');"></div>
                        </div>
                        <div class="home-stories__second-slide">
                            <div class="home-stories-slide__image" style="background-image: url('http://today.ttu.edu/posts/2017/10/Images/ligo/LIGO-Corsi-2017-Conception-lg.jpg');"></div>
                        </div>
                    </div>

                    <div class="home-stories__text-slider">
                        <div class="home-text__slide">
                            <h3><a href="http://today.ttu.edu/posts/2017/10/ligo">LIGO Scientists' New Finding Could 'Change the Way We Do Astronomy'</a></h3>
                        </div>
                        <div class="home-text__slide">
                            <h3><a href="http://today.ttu.edu/posts/2017/10/micro-gin">Fiber and Biopolymer Research Institute's Micro-Gin Mirrors Commercial Facilities</a></h3>
                        </div>
                        <div class="home-text__slide">
                            <h3><a href="http://today.ttu.edu/posts/2017/09/3d-blood-vessels">Engineer Developing Methods, Applications to Construct Blood Vessels Using 3-D Printing Technology</a></h3>
                        </div>
                    </div>
                </div>

            </section>

            <section id="ocm-home__photography">
                <div class="home-photography__background"></div>
                <h2 class="ocm-home__h2">Video &amp; Photography</h2>
                <a href="#" class="ocm__more-link">Campus Snapshots</a>
                <div class="home-photography__photo"></div>
                <video autoplay controls muted loop poster="images/will-rogers.jpg">
                    <source src="images/videos/GoinBandDedication.mp4" type="video/mp4">
                </video>
            </section>

            <section id="ocm-home__web">
                <h2 class="ocm-home__h2">Web &amp;<br />Social Media</h2>
                <div class="home-web__cards">
                    <div class="home-web__card home-web__card--desktop">
                        <div class="web-card__face web-card__front"></div>
                        <div class="web-card__face web-card__back"></div>
                        <div class="web-card__face web-card__left"></div>
                        <div class="web-card__face web-card__right"></div>
                        <div class="web-card__face web-card__top"></div>
                        <div class="web-card__face web-card__bottom"></div>
                    </div>
                    <div class="home-web__card home-web__card--tablet">
                        <div class="web-card__face web-card__front"></div>
                        <div class="web-card__face web-card__back"></div>
                        <div class="web-card__face web-card__left"></div>
                        <div class="web-card__face web-card__right"></div>
                        <div class="web-card__face web-card__top"></div>
                        <div class="web-card__face web-card__bottom"></div>
                    </div>
                    <div class="home-web__card home-web__card--mobile">
                        <div class="web-card__face web-card__front"></div>
                        <div class="web-card__face web-card__back"></div>
                        <div class="web-card__face web-card__left"></div>
                        <div class="web-card__face web-card__right"></div>
                        <div class="web-card__face web-card__top"></div>
                        <div class="web-card__face web-card__bottom"></div>
                    </div>
                </div>
                <div class="home-web__background"></div>
                <a href="#" class="ocm__more-link">More<br />Web Projects</a>
            </section>

            <section id="ocm-home__work-with-us">
                <h2 class="ocm-home__h2">Work<br />With Us</h2>
                <div class="ocm-home-wwu__link">
                    <a href="/work-with-us/">Get Started</a>
                    <div class="ocm-home-wwu-box__bottom"></div>
                    <div class="ocm-home-wwu-box__left"></div>
                    <div class="ocm-home-wwu-box__top"></div>
                    <div class="ocm-home-wwu-box__right"></div>
                </div>
            </section>
        <!-- CONTENT END -->




        <?php include 'includes/ttu-body-bottom.php'; ?>
    </body>
</html>
