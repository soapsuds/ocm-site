'use strict';

$(document).ready(function () {

  /***** INITIALIZING SLIDERS *****/
  $('.home-stories__main-slider').slick({
    asNavFor: '.home-stories__second-slider,.home-stories__text-slider',
    autoplay: true,
    autoplaySpeed: 5000
  });

  $('.home-stories__second-slider').slick({
    arrows: true,
    asNavFor: '.home-stories__main-slider,.home-stories__text-slider',
    fade: true
  });

  $('.home-stories__text-slider').slick({
    arrows: false,
    asNavFor: '.home-stories__main-slider,.home-stories__second-slider',
    fade: true
  });

  $('.ttu-emergency-home-alert__text').slick({
    arrows: false,
    dots: true,
    autoplay: true,
    autoplaySpeed: 10000,
    adaptiveHeight: true
  });

  /***** HOME WEB CARDS *****/
  var desktopCard = $('.home-web__card--desktop');
  var tabletCard = $('.home-web__card--tablet');
  var mobileCard = $('.home-web__card--mobile');
  var webWorks = ['ttu-homepage', 'ansel-adams', 'presidents-report', 'rec-center'];

  var webWorksIndex = 0;

  // Initally setting the cards' backgrounds
  setCardBackground(desktopCard, webWorks[webWorksIndex], 'desktop');
  setCardBackground(tabletCard, webWorks[webWorksIndex], 'tablet');
  setCardBackground(mobileCard, webWorks[webWorksIndex], 'mobile');

  // Every 12 seconds, change the works
  setInterval(function () {

    if (webWorksIndex >= webWorks.length - 1) {
      webWorksIndex = 0;
    } else {
      webWorksIndex++;
    }

    // Change the cards' backgrounds again
    setCardBackground(desktopCard, webWorks[webWorksIndex], 'desktop');
    setCardBackground(tabletCard, webWorks[webWorksIndex], 'tablet');
    setCardBackground(mobileCard, webWorks[webWorksIndex], 'mobile');
  }, 12000);
});

/***** WEB CARD FUNCTIONS *****/
/**
 * Sets the background of a card
 *
 * @param {$Object} $card - The jQuery object of a card
 * @param {String}  image - The image name
 * @param {String}  size  - Which card this is [desktop, tablet, mobile]
 */
function setCardBackground($card, image, size) {
  var cardFront = $card.find('.web-card__front');

  // Changing background
  cardFront.css({ 'background-image': 'url(/images/home/web/' + image + '-' + size + '.jpg)' });
}

$(function () {

  $('.submit').click(function () {

    $('.ocm-wwu-project-progress__step').toggleClass('is-complete');

    // Deactivate the default behavior of going to the next page on click
    return false;
  });
});