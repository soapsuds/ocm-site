/*****************
   Logo Scripts
*****************/
$(document).ready(function(){
  $('.ocm-logo__form button').on('click', function(e) {
    e.preventDefault();
    validateForm($('.ocm-logo__form'));
  });
});

/**
 * Submits the form to request a new logo
 *
 * @param {string} token - Google invisible ReCaptcha token
 */
function submitNewLogoForm(token) {
    // Submitting form
    $('.ocm-logo__form').submit();
};
