$(document).ready(function(){
  $('.ocm-event-announcement__form button').on('click', function(e) {
    e.preventDefault();
    validateForm($('.ocm-event-announcement__form'));
  });
});

/**
 * Submits the form to request a new logo
 *
 * @param {string} token - Google invisible ReCaptcha token
 */
function submitNewEventAnnouncement(token) {
    // Submitting form
    $('.ocm-event-announcement__form').submit();
};
