'use strict';

/**
 * Validates form fields
 *
 * @param  {object} form    - The form to be validated
 * @return {bool}   isValid - Boolean to whether the form validated
 */
function validateForm(form) {
  var isValid = true;

  // Text fields
  form.find('input[type=text]').each(function () {
    // Removing any error messages
    $(this).removeClass('error');
    $(this).next('.ocm__error-tip').fadeOut(300);

    // If field is empty
    if ($(this).val() === '' && $(this).prop('required')) {
      // Adding error class to turn field red
      $(this).addClass('error');
      // Adding the hint for the error
      $(this).next().html('Please enter a value.');
      $(this).next('.ocm__error-tip').fadeIn(300);
      // Setting to isValid to false
      isValid = false;
    }
  });

  // Textareas
  form.find('textarea').each(function () {
    // Removing any error messages
    $(this).removeClass('error');
    $(this).next('.ocm__error-tip').fadeOut(300);

    // If field is empty
    if ($(this).val() === '' && $(this).prop('required')) {
      // Adding error class to turn field red
      $(this).addClass('error');
      // Adding the hint for the error
      $(this).next().html('Please enter a value.');
      $(this).next('.ocm__error-tip').fadeIn(300);
      // Setting to isValid to false
      isValid = false;
    }
  });

  // Email fields
  form.find('input[type=email]').each(function () {
    var value = $(this).val();
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    // Removing any error messages
    $(this).removeClass('error');
    $(this).next('.ocm__error-tip').fadeOut(300);

    // If field is empty
    if (value === '' && $(this).prop('required')) {
      // Adding error class to turn field red
      $(this).addClass('error');
      // Adding the hint for the error
      $(this).next().html('Please enter a value.');
      $(this).next('.ocm__error-tip').fadeIn(300);
      // Setting to isValid to false
      isValid = false;
    } else if (!re.test(value)) {
      // If it's not a valid email
      // Adding error class to turn field red
      $(this).addClass('error');
      // Adding the hint for the error
      $(this).next().html('Please enter a valid email.');
      $(this).next('.ocm__error-tip').fadeIn(300);
      // Setting isValid to false
      isValid = false;
    }
  });

  // Phone Numbers
  form.find('input[type=tel]').each(function () {
    // Removing any error messages
    $(this).removeClass('error');
    $(this).next('.ocm__error-tip').fadeOut(300);

    // If field is empty
    if ($(this).val() === '' && $(this).prop('required')) {
      // Adding error class to turn field red
      $(this).addClass('error');
      // Adding the hint for the error
      $(this).next().html('Please enter a value.');
      $(this).next('.ocm__error-tip').fadeIn(300);
      // Setting to isValid to false
      isValid = false;
    }
  });

  // Date fields
  form.find('input[type=date]').each(function () {
    // Removing any error messages
    $(this).removeClass('error');
    $(this).next('.ocm__error-tip').fadeOut(300);

    var splitDate = $(this).val().split("-");
    var year = parseInt(splitDate[0], 10);
    var month = parseInt(splitDate[1], 10);
    var day = parseInt(splitDate[2], 10);

    var currentDate = new Date();
    var inputDate = new Date($(this).val());

    // If they haven't changed the date
    if ($(this).val() === '' && $(this).prop('required')) {
      // Adding error class to turn field red
      $(this).addClass('error');
      // Adding the hint for the error
      $(this).next().html('Please enter a value.');
      $(this).next('.ocm__error-tip').fadeIn(300);
      // Setting to isValid to false
      isValid = false;

      // If they've selected a date in the past
    } else if (inputDate.setHours(0, 0, 0, 0) < currentDate.setHours(0, 0, 0, 0) && $(this).prop('required')) {
      // Adding error class to turn field red
      $(this).addClass('error');
      // Adding the hint for the error
      $(this).next().html('Please enter a date in the future.');
      $(this).next('.ocm__error-tip').fadeIn(300);
      // Setting to isValid to false
      isValid = false;
    }
  });

  // Time
  form.find('input[type=time]').each(function () {
    // Removing any error messages
    $(this).removeClass('error');
    $(this).next('.ocm__error-tip').fadeOut(300);

    // If field is empty
    if ($(this).val() === '' && $(this).prop('required')) {
      // Adding error class to turn field red
      $(this).addClass('error');
      // Adding the hint for the error
      $(this).next().html('Please enter a value.');
      $(this).next('.ocm__error-tip').fadeIn(300);
      // Setting to isValid to false
      isValid = false;
    }
  });

  if (!isValid) {
    // Re-enabling the form
    grecaptcha.reset();
    enableForm(form);
  } else {
    grecaptcha.execute();
  }

  return isValid;
}

/**
 * Disables a form while something is working
 *
 * @param {object} form - The form to be disabled
 */
function disableForm(form) {
  // Fading form
  form.fadeTo('fast', 0.4);
  // Disabling form fields and buttons
  form.find('input').attr('disabled', 'disabled');
  form.find('button').disabled = true;
  // Changing text in button
  form.children('button').html('Working...');
}

/**
 * Enables a form while something is working
 *
 * @param {object} form - The form to be enabled
 */
function enableForm(form) {
  // Fading form
  form.fadeTo('fast', 1);
  // Enabling form fields
  form.find('input').removeAttr('disabled');
  form.find('button').disabled = false;
  // Changing text in button
  form.children('button').html('Send');
}